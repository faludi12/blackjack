/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjackclient;
import blackjack.card.Card;
import blackjackclient.gui.WelcomeRunner;
import blackjackclient.handlers.CardBuffer;
import blackjackclient.handlers.CardHandler;
import blackjackclient.member.Member;
import blackjackclient.member.Dealer;
import blackjackclient.member.Opponent;
import blackjackclient.member.Player;
import java.io.IOException;
import java.util.LinkedList;
import javafx.util.Pair;
/**
 *
 * @author pbudahazy
 */
public final class Gambling {
    private final String GEP = "127.0.0.1";
    private final int PORT = 12345;
    private Member player;
    private Member dealer;
    private CardHandler cardHandler;
    private final LinkedList<Member> members ;
    private GUIRunner guiRunner ;
    
    private String name="";   
    private final WelcomeRunner wr;
    boolean hit = false;
    boolean stay = false;
    private final LinkedList<Pair<String,Card>> buffer;
    private CardBuffer cardBuffer;

    /**
     *
     * @param name
     * @param wr
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Gambling(String name,WelcomeRunner wr) throws IOException, ClassNotFoundException {
        this.members = new LinkedList();
        this.buffer = new LinkedList<>();
        this.name = name;
        this.wr = wr;
        this.runGame();
    }

    private void createPlayerersAndDealer() throws IOException, ClassNotFoundException{
        
        
        dealer = new Dealer();
        player = new Player(name,GEP,PORT);
        this.add();
        wr.stop();
        guiRunner = new GUIRunner(members,this);
        new Thread(guiRunner).start();
        
    }
    
    private void add() throws IOException, ClassNotFoundException{
        this.addMember(player);
        this.getOpponents();
        this.addMember(dealer);
    }
    
    
    
    private void getOpponents() throws IOException, ClassNotFoundException{
        LinkedList<String> OpponentsName = player.getSocket().getOpponents();
        for(String str : OpponentsName){
            if(!str.equals(player.getName())){
                this.addMember(new Opponent(str));
            } 
        } 
    }
    
    private void addMember(Member member){
        if(!members.contains(member)){
           members.add(member);
           System.out.println(member.getName()); 
        }
        
    }

    private void setCardHandler() {
        cardHandler = new CardHandler(members);
        cardBuffer = new CardBuffer(this,cardHandler);
        cardBuffer.start();
        
    }
    
    private CardHandler getCardHandler() {
        return cardHandler;
    }

    private LinkedList<Member> getMembers() {
        return members;
    }
    
    /**
     *
     */
    public void printMembersHand(){
        members.stream().map((member) -> {
            System.out.println(member.getName() + "'s cards:");
            return member;
        }).forEachOrdered((member) -> {
            member.printHand();
        });
    }

    /**
     *
     * @return
     */
    public Member getPlayer() {
        return player;
    }
    
    /**
     *
     * @param card
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void addBuffer(Pair<String,Card> card) throws IOException, ClassNotFoundException{
        buffer.add(card);
        System.out.println(card.toString());
        switch (card.getKey()) {
            case "":
                newGame();
                guiRunner.getGui().newGame();
                break;
            case "Nobody":
                guiRunner.getGui().printCommand(card);
                break;
            default:
                cardHandler.findMemberAndDealCard(card);
                break;
        }
        if(card.getValue().getNumber() < 0 && card.getKey().equals(getDealer().getName())){
            guiRunner.getGui().showSecondCard();
        }
        if(card.getValue().getNumber() == -5 || card.getValue().getNumber() == -8){
            guiRunner.getGui().endGame();
            endGame();
        }
        
        if(card.getValue().getNumber() == -7){
            Member thisMember = findMember(card.getKey());
            members.remove(thisMember);
            if(thisMember.isActual()){
                thisMember.setActual(false);
                thisMember.setBusted(true);
            }
        }
        
        if(card.getValue().getNumber() > 0){
          guiRunner.getGui().refresh(card);  
        }else{
            guiRunner.getGui().printCommand(card);
        }
        
        if(player.isActual()){
            guiRunner.getGui().yourTurn();
        }else{
            guiRunner.getGui().notYourTurn();
        }
        
        
    }
    
    /**
     *
     */
    public void endGame(){
        members.forEach((member) -> {
            member.emptyHand();
            member.setBusted(false);
        });
        buffer.clear();
        
    }
    
    public void sendAnswer(String answer){
        
    }
    
    public void exit(){
        cardBuffer.stop();
    }
    
    public void newGame() throws IOException, ClassNotFoundException{
        cardBuffer.stop();
        members.remove(dealer);
        getOpponents();
        members.add(dealer);
        guiRunner.getGui().addNameLabel();
        setCardHandler();
        
    }
    
    /**
     *
     * @return
     */
    public Member getDealer(){
        for(Member member : members){
            if(member.getName().equals("Dealer")){
                return member;
            }
        }
        return null;
    }
    
    /**
     *
     * @param name
     * @return
     */
    public Member findMember(String name){
        for(Member member : members){
            if(member.getName().equals(name)){
                return member;
            }
        }
        return null;
    }
    
    /**
     *
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void runGame() throws IOException, ClassNotFoundException{
        this.createPlayerersAndDealer();
        this.setCardHandler();
        this.getCardHandler().printMember();
        this.printMembersHand();
    }
}
