/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjackclient;

import blackjack.ConnectionHandler;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pbudahazy
 */
public class Timer implements Runnable{
    Thread runner ;
    private final AtomicBoolean running = new AtomicBoolean(false);  
    private int time;
    private int ogTime;
    ClientGUI gui;
    

    public Timer(int time,ClientGUI gui) {
        this.time = time;
        ogTime = time;
        this.gui = gui;
    }
    
    public void countDown() throws InterruptedException{
        Thread.sleep(1000);
        this.time--;
        gui.Timer(time);
        if(time == 0){
            
            stop();
            return;
        }
        
    }

    public boolean getRunning() {
        return running.get();
    }
    
    
    
    public void start(){
        runner = new Thread(this);
        runner.start();
        
        
    }
    
    
    
    /**
     *
     */
    public synchronized void stop(){
        
        running.set(false);
        
    }
    
    @Override
    public void run() {
        time = ogTime;
        running.set(true);
        do{
            try {
                countDown();
            } catch (InterruptedException ex) {
                Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }while(running.get());
        
        
        
        
    }
}
