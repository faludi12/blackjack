/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjackclient;

import blackjackclient.member.Member;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author pbudahazy
 */
public class GUIRunner implements Runnable {
    
    ClientGUI gui ;
    LinkedList<Member> members;
    Runnable guiRunner;
    Thread runner;
    final AtomicBoolean running = new AtomicBoolean(false);
    Gambling gambling;
   
    /**
     *
     * @param members
     * @param gambling
     */
    public GUIRunner(LinkedList<Member> members,Gambling gambling) {
        this.gambling = gambling;
        this.members = members;
        gui = new ClientGUI(this.members,gambling);
    }
    
    /**
     *
     */
    public void start(){
        runner = new Thread(this);
        runner.start();
    }
    
    /**
     *
     */
    public void stop(){
        running.set(false);
    }
    
    /**
     *
     * @return
     */
    public synchronized ClientGUI getGui() {
        return gui;
    }

    
    public void run(){
        running.set(true);
        while(running.get()){
            
        }
        
    }
}
