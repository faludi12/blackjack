package blackjackclient;

import blackjack.card.Card;
import blackjackclient.member.Member;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JFrame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

final class ClientGUI extends JFrame
{
    public final Dimension SCREENSIZE = Toolkit.getDefaultToolkit().getScreenSize();
    public final Dimension WINDOWSIZE = new Dimension(SCREENSIZE.width,SCREENSIZE.height-30);
    public final Color TABLECOLOR = new Color(7,99,36);
    public final Color BUTTONCOLOR = new Color(240,186,0);
    
    
    
    private final LinkedList<Card> usedCards;
    private final LinkedList<Pair<String,LinkedList<Rectangle>>> playerCardRectangle;
    private LinkedList<Pair<Member,Rectangle>> namePoints;
    private final LinkedList<Member> members;
    private final Gambling gambling;
    private final LinkedList<JLabel> nameLabels;
    
    private Board board;
    

    
    
   
    //Dimension cardTableGridDimension = new Dimension(120,900);
    private Dimension cardTableGridDimension;
    private Point cardTablePoint;
    private Rectangle cardTableGrid;
   
   
    private Dimension hitStayDimension;
    private Point hitStayPoint;
    private Rectangle hitStayGrid;
   
    private Dimension historyGridDimension;
    private Point historyPoint;
    private Rectangle historyGrid;
   
    private Dimension yesNoGridDimension;
    private Point yesNoPoint;
    private Rectangle yesNoGrid;
   
    private Dimension buttonDimension;
    private Point hitButtonPoint;
    private Point stayButtonPoint;
    private Point yesButtonPoint;
    private Point noButtonPoint;
    private Rectangle hitButtonRectangle;
    private Rectangle stayButtonRectangle;
    private Rectangle yesButtonRectangle;
    private Rectangle noButtonRectangle;

    private Dimension cardDimension;
    private Dimension nameDimension;
    private Dimension commandDimension;
    

    //Betűstílus
    private Font fontButton;
    private Font fontName ;
    
    //Gombok
    private JButton buttonHit ;
    private JButton buttonStay;
    private JButton buttonYes ;
    private JButton buttonNo ;
    private JButton buttonBid;
    private URL backUrl ;
    private URL iconUrl;
    private URL    loadUrl ;
    private Icon   loadGif ;
    private Icon   backImg ;
    private ImageIcon gameIcon;
    
    
    private JLabel backLabel;
    private JLabel secondCard;
    private JLabel continueLabel;
    private JLabel loadingLabel;
    private JLabel commandLabel;
    private JLabel timeLabel;
    
    private Timer timer;
    
    private JComboBox bid;
    
    
    ClientGUI(LinkedList<Member> members,Gambling gambling) {
       
        
        this.playerCardRectangle = new LinkedList<>();
        usedCards = new LinkedList<>();
        this.gambling = gambling;
        this.members = members;
        nameLabels = new LinkedList<>();
        init();
        this.setCardsPoint(this.members);
      
    }
    
    public void init(){
        iconUrl = getClass().getResource("./img/gaming.png");
        gameIcon = new ImageIcon(iconUrl);
        cardTableGridDimension = new Dimension(770,800);
        cardTablePoint = new Point(50,50 );
        cardTableGrid = new Rectangle(cardTablePoint,cardTableGridDimension );
        hitStayDimension = new Dimension(110, 120);
        hitStayPoint = new Point(50 + SCREENSIZE.width-400 + 100 ,50);
        hitStayGrid = new Rectangle(hitStayPoint,hitStayDimension);
        historyGridDimension = new Dimension(300,120);
        historyPoint = new Point(50,WINDOWSIZE.height - 200);
        historyGrid = new Rectangle(historyPoint,historyGridDimension);
        yesNoGridDimension = new Dimension ( 220,100);
        yesNoPoint = new Point(hitStayPoint.x + 5,WINDOWSIZE.height - 200);
        yesNoGrid = new Rectangle(yesNoPoint,yesNoGridDimension);
        buttonDimension = new Dimension(100,40);
        hitButtonPoint = new Point(hitStayPoint.x + 5 ,hitStayPoint.y + 5);
        stayButtonPoint = new Point(hitButtonPoint.x  ,hitButtonPoint.y + buttonDimension.height +30);
        yesButtonPoint = new Point(yesNoPoint.x + 5,yesNoPoint.y + 50) ;
        noButtonPoint = new Point(yesButtonPoint.x + 10 + buttonDimension.width,yesNoPoint.y + 50) ;
        hitButtonRectangle = new Rectangle(hitButtonPoint,buttonDimension);
        stayButtonRectangle = new Rectangle(stayButtonPoint  ,buttonDimension);
        yesButtonRectangle  = new Rectangle(yesButtonPoint, buttonDimension) ;
        noButtonRectangle   = new Rectangle(noButtonPoint,buttonDimension) ;
        cardDimension = new Dimension(67 ,97);
        nameDimension = new Dimension(100,20);
        commandDimension = new Dimension(300,30);
        
        fontButton = new Font("Times New Roman",Font.PLAIN,20);
        fontName = new Font("Times New Roman",Font.PLAIN,15);
        buttonHit = new JButton("HIT");
        buttonStay = new JButton("STAY");
        buttonYes = new JButton("YES");
        buttonNo = new JButton("NO");
        this.setIconImage(gameIcon.getImage());

        this.setSize(WINDOWSIZE);
        this.setTitle("BlackJack");
        this.setBackground(TABLECOLOR);
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        board = new Board();
        this.setContentPane(board);
        this.setLayout(null);
        //Hit Button
        ActionHit aHit = new ActionHit();
        buttonHit.addActionListener(aHit);
        buttonHit.setBounds(hitButtonRectangle);
        buttonHit.setBackground(BUTTONCOLOR);
        buttonHit.setFont(fontButton);
        //Stay Button
        ActionStay aStay = new ActionStay();
        buttonStay.addActionListener(aStay);
        buttonStay.setBounds(stayButtonRectangle);
        buttonStay.setBackground(BUTTONCOLOR);
        buttonStay.setFont(fontButton);
        //Yes Button 
        ActionYes aYes = new ActionYes();
        buttonYes.addActionListener(aYes);
        buttonYes.setBounds(yesButtonRectangle);
        buttonYes.setBackground(BUTTONCOLOR);
        buttonYes.setFont(fontButton);
        //No Button
        ActionNo aNo = new ActionNo();
        buttonNo.addActionListener(aNo);
        buttonNo.setBounds(noButtonRectangle);
        buttonNo.setBackground(BUTTONCOLOR);
        buttonNo.setFont(fontButton);
        //backSide
        backUrl = getClass().getResource("./img/backside.png");
        backImg = new ImageIcon(backUrl);
        backLabel = new JLabel(backImg);
        loadUrl = getClass().getResource("./img/ajax-loader.gif");
        loadGif = new ImageIcon(loadUrl);
        loadingLabel = new JLabel(loadGif);
        loadingLabel.setBounds(hitStayGrid);
        //board.add(loadingLabel);  
        
        continueLabel = new JLabel("Continue?");
        continueLabel.setBounds(yesNoGrid.x, yesNoGrid.y, yesNoGrid.width/2, 20);

        continueLabel.setFont(new Font("Times New Roman",Font.PLAIN,20));
        continueLabel.setForeground(BUTTONCOLOR);
        commandLabel = new JLabel();
        commandLabel.setBounds(hitStayGrid.x,hitStayGrid.y + hitStayGrid.height + 10 , commandDimension.width, commandDimension.height);
        commandLabel.setFont(fontName);
        commandLabel.setForeground(BUTTONCOLOR);
        timeLabel = new JLabel();
        timeLabel.setBounds(continueLabel.getBounds().x + continueLabel.getBounds().width , continueLabel.getBounds().y, 20 ,continueLabel.getBounds().height);
        timeLabel.setFont(continueLabel.getFont());
        timeLabel.setForeground(BUTTONCOLOR);
        timer = new Timer(5,this);
        bid = new JComboBox();
        bid.setBounds(hitStayGrid.x,hitStayGrid.y,50,40);
        bid.setBackground(BUTTONCOLOR);
        buttonBid = new JButton("Bid");
        buttonBid.setFont(buttonYes.getFont());
        buttonBid.setBackground(BUTTONCOLOR);
        buttonBid.setBounds(bid.getBounds().x + bid.getBounds().width + 5, bid.getBounds().y, 70, bid.getBounds().height);
        ActionBid aBid = new ActionBid();
        buttonBid.addActionListener(aBid);
        addNameLabel();
        makeBid();
       
    }

 
    
    public class Board extends JPanel{
        @Override
        public void paintComponent(Graphics g){
            g.setColor(TABLECOLOR);
            g.fillRect(0, 0, SCREENSIZE.width, SCREENSIZE.height);
            g.setColor(Color.BLACK);
            g.drawRect(cardTableGrid.x,cardTableGrid.y,cardTableGrid.width,cardTableGrid.height);
            g.setColor(Color.BLACK);
            g.drawRect(historyGrid.x,historyGrid.y,historyGrid.width,historyGrid.height);
            g.setColor(Color.BLACK);
            g.drawRect(hitStayGrid.x,hitStayGrid.y,hitStayGrid.width,hitStayGrid.height);
            g.setColor(Color.BLACK);
            g.drawRect(yesNoGrid.x,yesNoGrid.y,yesNoGrid.width,yesNoGrid.height);   
        }   
    }
    
    public void makeBid(){
        board.remove(loadingLabel);
        board.repaint();
        setBidList();
        board.add(bid);
        board.add(buttonBid);
        board.repaint();
    }
    
    public void load(){
        board.remove(bid);
        board.remove(buttonBid);
        board.repaint();
        board.add(loadingLabel);
        board.repaint();
        
    }
    
    public void setBidList(){
        bid.removeAllItems();
        for(int i = 0;i < gambling.getPlayer().getCoins();i += 5){
            bid.addItem(Integer.toString(i));
        }
        
    }
    
    
    
    public void yourTurn(){
        board.remove(loadingLabel);
        board.add(buttonHit);
        board.add(buttonStay);
        board.repaint();
    }
    
    public void notYourTurn() {
        board.remove(buttonHit);
        board.remove(buttonStay);
        board.add(loadingLabel);
        board.repaint();
    }
    
    public synchronized void refresh(Pair<String,Card> card) throws IOException{
        
        if(gambling.getDealer().getMyCards().size() > 1 && card.getValue().equals(gambling.getDealer().getMyCards().get(1))){
            hideSecondCard(card);
            return;
        }
        if(gambling.getDealer().getMyCards().size() > 2){
           showSecondCard();
        }
        URL url = getClass().getResource(card.getValue().getImageUrl());
        Icon imgIcon = new ImageIcon(url);
        JLabel cardPict = new JLabel(imgIcon);
        cardPict.setBounds(getPoints(card));
        board.add(cardPict);
        board.repaint();
    }
    
    public void hideSecondCard(Pair<String,Card> card) throws IOException{
        
        URL url = getClass().getResource(card.getValue().getImageUrl());
        Icon imgIcon = new ImageIcon(url);
        secondCard = new JLabel(imgIcon);
        Rectangle rect = getPoints(card);
        secondCard.setBounds(rect);
        backLabel.setBounds(rect);
        board.add(backLabel);
        board.repaint();
    }
    
    public void showSecondCard(){
        board.remove(backLabel);
        board.add(secondCard);
        board.repaint();
    }
    
    public Rectangle getPoints(Pair<String,Card> card){
        for(Pair<String,LinkedList<Rectangle>> rect : playerCardRectangle){
            if(card.getKey().equals(rect.getKey())){
                Rectangle rectangle = rect.getValue().getFirst();
                rect.getValue().removeFirst();
                return rectangle;
            }
        }
        return null;
    }
    
    public void addNameLabel(){
        for (int i = 0; i < members.size() ; i++){
            JLabel label = new JLabel();
            if(i == 0){
                label.setText("Your cards:");
            }else{
                label.setText(members.get(i).getName()+"'s cards:");
            }
            label.setFont(fontName);
            label.setForeground(BUTTONCOLOR);
            label.setBounds(5,cardTableGrid.y + 10 +cardDimension.height/2 + i * (cardDimension.height + 10)   ,nameDimension.width,nameDimension.height);
            nameLabels.add(label); 
            board.add(label);
            
            
        }
        board.repaint();
    }
    
    public void setNamePanelPoint(){
        for (int i = 0; i < members.size() ; i++){
            JLabel label = new JLabel(members.get(i).getName());
            label.setFont(fontName);
            label.setForeground(BUTTONCOLOR);
            if(!nameLabels.contains(label)){
                nameLabels.add(label);
            }
            
        }
    }
    
    public void setCardsPoint(LinkedList<Member> members){
        String name ;
        
        int j = 0;
        for (Member member : members){
            LinkedList<Rectangle> rectangle = new LinkedList<>();
            name = member.getName();
            for(int i = 0; i < 10 ; i++){
                rectangle.add(new Rectangle(50 + cardTableGrid.x  + i * (10 + cardDimension.width)   , cardTableGrid.y + 10 + j * (cardDimension.height + 10)  ,cardDimension.width,cardDimension.height));
            }
            j++;
            playerCardRectangle.add(new Pair(name,rectangle));
        }
    }
    
    public void printCommand(Pair<String,Card> command){
        String numStr = "";
        try{
            board.remove(commandLabel);
        }catch(NullPointerException ex){
            
        }
        switch(command.getValue().getNumber()){
            
            case -7:
                numStr = " leaved the game";
                break;
            
            case -6: 
                commandLabel.setText("New Game");
                board.add(commandLabel);
                return;

            case -5: 
                numStr = " is the winner";
                break;
                
            case -4: 
                numStr = " is busted";
                break;
                
            case -3: 
                numStr = " hit a card";
                break;
                
            case -2: 
                numStr = " stayed";
                break;
                
            case -1: 
                numStr = "'s turn";
                break; 
        }
        commandLabel.setText(command.getKey() + numStr);
        board.add(commandLabel);
    }
    
    public void endGame(){
        
        timer.start();
        board.add(buttonYes);
        board.add(buttonNo);
        board.add(continueLabel);
        board.repaint();
    }
    
    public void newGame(){
        board.removeAll();
        playerCardRectangle.clear();
        setCardsPoint(members);
        addNameLabel();
        makeBid();
    }
    
    public class ActionHit implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            try {
                String ans = "H";
                gambling.getPlayer().getSocket().writeOutput(ans);
                if(gambling.getPlayer().isBusted()){
                    ans = "B";
                    gambling.getPlayer().getSocket().writeOutput(ans);
                    gambling.getPlayer().setActual(false);
                    notYourTurn();
                }
            }catch(IOException ex) {
                Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public class ActionStay implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            String ans = "S";
            try {
                gambling.getPlayer().getSocket().writeOutput(ans);
                gambling.getPlayer().setActual(false);

            } catch (IOException ex) {
                Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            notYourTurn();
        }
    }
    
    public class ActionYes implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            keepPlaying();
        }
    }
    
    public class ActionNo implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            leave();
                       
        }
    }
    
    public class ActionBid implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            String ans = (String) bid.getSelectedItem();
            try {
                gambling.getPlayer().getSocket().writeOutput(ans);
            } catch (IOException ex) {
                Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            load();
                       
        }
    }
    
    public void Timer(int time){
        if(!timer.getRunning()){
            return;
        }
        board.remove(timeLabel);
        timeLabel.setText(Integer.toString(time));
        System.err.println(Integer.toString(time));
        board.add(timeLabel);
        board.repaint();
        if(time == 0){
            leave();
        }
        
       // board.remove(timeLabel);
    }
    
    private void keepPlaying(){
        timer.stop();
        
        String ans = "P";
        try {
            gambling.getPlayer().getSocket().writeOutput(ans);
        } catch (IOException ex) {
            Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        board.removeAll();
        board.add(loadingLabel);
        commandLabel.setText("Waiting for the server");
        board.add(commandLabel);
        board.repaint();
               
    }
    
    private void leave(){
        timer.stop();
        String ans = "E";
        
            try {
                gambling.getPlayer().getSocket().writeOutput(ans);
            } catch (IOException ex) {
                Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            board.removeAll();
            commandLabel.setText("YOU LEFT THE GAME");
            board.add(commandLabel);
            board.repaint();
            try {
                gambling.exit();
                gambling.getPlayer().getSocket().closeSocket();
            } catch (IOException ex) {
                Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
}