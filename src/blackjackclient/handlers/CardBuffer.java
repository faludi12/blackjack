/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjackclient.handlers;

import blackjack.card.Card;
import blackjackclient.Gambling;
import java.io.IOException;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;

/**
 *
 * @author pbudahazy
 */
public class CardBuffer implements Runnable{
    Thread runner ;
    private final AtomicBoolean running = new AtomicBoolean(false);  
    LinkedList<Pair<String,Card>> buffer;
    Gambling gambling;
    CardHandler cardHandler;
    
    /**
     *
     * @param gambling
     * @param cardHandler
     */
    public CardBuffer(Gambling gambling,CardHandler cardHandler) {
        this.buffer = new LinkedList<>();
        this.gambling = gambling;
        this.cardHandler = cardHandler;
    }
    
    /**
     *
     */
    public void readCard() {
        try { 
                gambling.addBuffer(gambling.getPlayer().getSocket().readInput());

        } catch (ClassNotFoundException | IOException ex) {
            
        }
    }
    
    /**
     *
     */
    public synchronized void stop(){
        running.set(false);
    }
    
    /**
     *
     */
    public void start(){
        runner = new Thread(this);
        
        runner.start();
    }
    @Override
    public void run() {
        running.set(true);
        while(running.get()){
            readCard();
            
            
        }
    }
    
}
