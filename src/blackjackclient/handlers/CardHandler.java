/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjackclient.handlers;

import blackjack.card.Card;
import blackjack.card.Suit;
import blackjackclient.member.Member;
import java.io.IOException;
import java.util.LinkedList;
import javafx.util.Pair;


/**
 *
 * @author pbudahazy
 */
public class CardHandler {
    
    private LinkedList<Member> members;

    /**
     *
     * @param members
     */
    public CardHandler(LinkedList<Member> members) {
        this.members = members;
    }
    
    /**
     *
     * @param pair
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public boolean findMemberAndDealCard(Pair<String,Card> pair) throws IOException, ClassNotFoundException{
        Member aMember = null;
        
        for(Member member : members){
            if(member.getName().equals(pair.getKey())){
                aMember = member;
                break;
            }
        }
        
        if(pair.getValue().equals(Card.ACTUAL)){
            aMember.setActual(true);
            System.out.println(aMember.getName() + "'s turn");
        }else if(pair.getValue().equals(Card.HIT)){
            System.out.println(aMember.getName() + " Hit a card:");
        }else if(pair.getValue().equals(Card.STAY)){
            aMember.setActual(false);
            System.out.println(aMember.getName() + " Stayed");
        }else if(pair.getValue().equals(Card.BUSTED)){
            System.out.println(aMember.getName() + " is busted.");
            aMember.setBusted(true);
            
        }else if(pair.getValue().equals(Card.WINNER)){
            System.out.println(aMember.getName() + " is the winner.");
            aMember.setWin(true);
        }else if(pair.getValue().equals(Card.DRAW)){
            System.out.println(aMember.getName() + "  Draw.");
            aMember.setWin(true);
        }else if(pair.getValue().getSuit().equals(Suit.Commmand) && pair.getValue().getNumber() > 0){
            aMember.setBid(pair.getValue().getNumber());
            System.out.println(pair.getKey() + "'s bid :" + pair.getValue().getNumber());
        }else{
            if(!aMember.addCard(pair.getValue())){
                System.out.println("Busted.");
                aMember.printHand();
                aMember.setBusted(true);
                aMember.setActual(false);
            }
                
        }
        
        return aMember.isBusted();
    }
    
    /**
     *
     */
    public void printMember(){
        members.forEach((member) -> {
            System.out.println(member.getName());
        });
    }
    
    
}
