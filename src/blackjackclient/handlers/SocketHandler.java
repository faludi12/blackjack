/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjackclient.handlers;

import blackjack.card.Card;
import blackjackclient.member.Member;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import javafx.util.Pair;
import java.util.LinkedList;

/**
 *
 * @author pbudahazy
 */
public class SocketHandler {
    
    private final Socket socket;
    private final ObjectInputStream input ;
    private final ObjectOutputStream output ;
    private Pair<String,String> answerPair ;
    private Member player;

    /**
     *
     * @param host
     * @param port
     * @throws IOException
     */
    public SocketHandler(String host,int port) throws IOException {
        socket = new Socket(host, port);
        output = new ObjectOutputStream(socket.getOutputStream());
        input = new ObjectInputStream(socket.getInputStream());  
    }

    /**
     *
     * @param answer
     * @throws IOException
     */
    public void writeOutput(String answer) throws IOException{  
        
        output.writeObject(new Pair(player.getName(),answer));
    }
  
    /**
     *
     * @return
     * @throws IOException
     */
    public Pair<String,Card> readInput() throws IOException{      
        Pair<String,Card> inp =  null;
        try{
            while(inp == null){ inp = (Pair<String,Card>)input.readObject();}
        }catch(ClassNotFoundException e){
            
        }
        
        return inp;
    }

    /**
     *
     * @param player
     */
    public void setPlayer(Member player) {
        this.player = player;
    }
    
    /**
     *
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public LinkedList<String> getOpponents() throws IOException, ClassNotFoundException{
        LinkedList<String> inp =  null; 
        try{
            while(inp == null){inp = (LinkedList<String>)input.readObject();}
        }catch(ClassNotFoundException ex){
            
        }
        
        return inp;
    }

    /**
     *
     * @return
     */
    public ObjectInputStream getInput() {
        return input;
    }

    /**
     *
     * @return
     */
    public ObjectOutputStream getOutput() {
        return output;
    }
    
    public void closeSocket() throws IOException{
        socket.close();
    }
    
    
}
