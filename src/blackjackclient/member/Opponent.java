/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjackclient.member;

import blackjack.card.Card;
import java.io.IOException;
/**
 *
 * @author pbudahazy
 */
public class Opponent extends Member{
    
    /**
     *
     * @param name
     * @throws IOException
     */
    public Opponent(String name) throws IOException {
        super(name);
    }
    
    /**
     *
     * @param card
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public boolean addCard(Card card) throws IOException, ClassNotFoundException{     
        myCards.add(card);
        return (super.getHandsum() <= 21);
    }
    
    
}
