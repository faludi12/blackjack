/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjackclient.member;

import blackjack.card.Card;
import java.io.IOException;
import blackjackclient.handlers.SocketHandler;
/**
 *
 * @author pbudahazy
 */
public class Player extends Member{

    /**
     *
     * @param name
     * @param host
     * @param port
     * @throws IOException
     */
    public Player(String name,String host,int port) throws IOException {
        super(name);
        
        socket = createSocket(host ,port);
        socket.getOutput().writeObject(name);
        setSocketsPlayer();
    }
    
    private SocketHandler createSocket(String host, int port) throws IOException {
        return new SocketHandler(host,port);
    }
    
    /**
     *
     * @param card
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public boolean addCard(Card card) throws IOException, ClassNotFoundException{
        myCards.add(card);
        return (super.getHandsum() <= 21);
    }
    
    /**
     *
     */
    public final void setSocketsPlayer(){
        socket.setPlayer(this);
    }
}
