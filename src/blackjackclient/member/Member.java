/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjackclient.member;

import blackjack.card.Card;
import java.io.IOException;
import java.util.LinkedList;
import blackjackclient.handlers.SocketHandler;
import java.util.Objects;

/**
 *
 * @author pbudahazy
 */
public abstract class Member {
    
    /**
     *
     */
    protected String name;

    /**
     *
     */
    protected LinkedList<Card> myCards;
    SocketHandler socket;
    private boolean actual = false;
    private boolean busted = false;
    private boolean win = false;
    private int coins = 50;
    private int bid = 0;

    /**
     *
     * @param name
     * @throws IOException
     */
    protected Member(String name) throws IOException {
        this.name = name;
        myCards = new LinkedList<>();
      //  socket = createSocket(host ,port);
        
    }
    
    /**
     *
     * @param card
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public abstract boolean addCard(Card card) throws IOException, ClassNotFoundException;
    
    /**
     *
     * @return
     */
    public int getHandsum() {
       int sum = 0;
       int numOfAces = 0;
       
       for(Card card : myCards){
           if(card.getNumber() == 14){
                numOfAces++;
                sum += 11;
                
           }else if(card.getNumber() > 10){
               sum += 10;
           }else{
               sum += card.getNumber();
           }
       }
       while(sum > 21 && numOfAces > 0){
           sum -= 10; 
           numOfAces--;
       }
       
       return sum;
       
    }
    
    /**
     *
     */
    public void printHand(){
       
        myCards.forEach((card) -> {
            System.out.printf(" %s\n",  card.toString());
        });
    } 

    /**
     *
     * @param showFirstCard
     */
    public void printHand(boolean showFirstCard){
        System.out.printf("%s's cards:\n",this.name);
        myCards.forEach((card) -> {
            if(card.equals(myCards.getFirst()) && !showFirstCard){
                System.out.println(" [hidden]");
            }else{
                System.out.printf(" %s\n",  card.toString());
            }
        });
    }
    
    /**
     *
     * @param isAct
     * @throws IOException
     */
    public void setActual(boolean isAct) throws IOException {
        this.actual = isAct;
        
    }
    
    /**
     *
     */
    public void emptyHand(){
        myCards.clear();
    }

    /**
     *
     * @return
     */
    public boolean isActual() {
        return actual;
    }

    /**
     *
     * @return
     */
    public boolean isBusted() {
        return busted;
    }

    /**
     *
     * @param busted
     */
    public void setBusted(boolean busted) {
        this.busted = busted;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public void setBid(int bid) {
        this.bid = bid;
        coins -= bid;
        
    }

    public int getCoins() {
        return coins;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Member other = (Member) obj;
        return Objects.equals(this.name, other.name);
    }

    /**
     *
     * @return
     */
    public boolean isWin() {
        return win;
    }

    /**
     *
     * @param win
     */
    public void setWin(boolean win) {
        this.win = win;
        if(win){
            coins += 2 * bid;
        }
        
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public LinkedList<Card> getMyCards() {
        return myCards;
    }

    /**
     *
     * @param myCards
     */
    public void setMyCards(LinkedList<Card> myCards) {
        this.myCards = myCards;
    }

    /**
     *
     * @return
     */
    public SocketHandler getSocket() {
        return socket;
    }

    /**
     *
     * @param socket
     */
    public void setSocket(SocketHandler socket) {
        this.socket = socket;
    }
    
    
    
    
    
    
}
