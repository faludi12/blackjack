/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjackclient.gui;

import blackjackclient.ClientRunner;
import blackjackclient.GUIRunner;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author pbudahazy
 */
public class WelcomeGui extends JFrame {
    
    /**
     *
     */
    public final Color BUTTONCOLOR = new Color(240,186,0);

    /**
     *
     */
    public final Color TABLECOLOR = new Color(7,99,36);

    /**
     *
     */
    protected Dimension frameDimension = new Dimension(600,200);

    /**
     *
     */
    protected URL url = WelcomeGui.class.getResource("ajax-loader.gif");

    /**
     *
     */
    protected Icon imgIcon = new ImageIcon(url);

    /**
     *
     */
    protected JLabel loadingLabel = new JLabel(imgIcon);

    /**
     *
     */
    protected JLabel welcomeLabel = new JLabel("Welcome Player!");

    /**
     *
     */
    protected JLabel infLabel = new JLabel("Please choose an option:");

    /**
     *
     */
    protected JLabel nameLabel = new JLabel("Please give a name!");
    
    /**
     *
     */
    protected JLabel waitLabel = new JLabel("Waiting for the other players");

    /**
     *
     */
    protected JLabel logNameLabel = new JLabel("Name:");

    /**
     *
     */
    protected JLabel logPassLabel = new JLabel("Password:");

    /**
     *
     */
    protected JButton accountButton = new JButton("I have an account");

    /**
     *
     */
    protected JButton guestButton = new JButton("Guest player");

    /**
     *
     */
    protected JButton createButton = new JButton("Create Account");

    /**
     *
     */
    protected JButton submitButton = new JButton("Submit");

    /**
     *
     */
    protected JButton loginButton = new JButton("Login");

    /**
     *
     */
    protected JButton signButton = new JButton("Sign up & Login");

    /**
     *
     */
    protected JButton backButton = new JButton("Back");

    /**
     *
     */
    protected JTextField nameText = new JTextField("Name");

    /**
     *
     */
    protected JPasswordField passText = new JPasswordField();

    /**
     *
     */
    protected JPanel namePanel = new JPanel();

    /**
     *
     */
    protected Window window = new Window();

    /**
     *
     */
    protected Dimension welcomeDimension = new Dimension(200,20);

    /**
     *
     */
    protected Point welcomePoint = new Point(frameDimension.width/2-welcomeDimension.width/2,20);

    /**
     *
     */
    protected Rectangle welcomeRectangle = new Rectangle(welcomePoint,welcomeDimension);

    /**
     *
     */
    protected Dimension infDimension = new Dimension(200,60);

    /**
     *
     */
    protected Point infPoint = new Point(frameDimension.width/2-infDimension.width/2,50);

    /**
     *
     */
    protected Rectangle infRectangle = new Rectangle(infPoint,infDimension);

    /**
     *
     */
    protected Dimension nameDimension = new Dimension(200,60);

    /**
     *
     */
    protected Point namePoint = new Point(frameDimension.width/2-welcomeDimension.width/2,0);

    /**
     *
     */
    protected Rectangle nameRectangle = new Rectangle(namePoint,nameDimension);

    /**
     *
     */
    protected Dimension waitDimension = new Dimension(200,20);

    /**
     *
     */
    protected Point waitPoint = new Point(frameDimension.width/2-waitDimension.width/2,58);

    /**
     *
     */
    protected Rectangle waitRectangle = new Rectangle(waitPoint,waitDimension);

    /**
     *
     */
    protected Dimension loadingDimension = new Dimension(24,24);

    /**
     *
     */
    protected Point loadingPoint = new Point(frameDimension.width/2-loadingDimension.width/2,frameDimension.height/2-loadingDimension.height/2);

    /**
     *
     */
    protected Rectangle loadingRectangle = new Rectangle(loadingPoint,loadingDimension);

    /**
     *
     */
    protected Dimension loginNameDimension = new Dimension(60,20); 

    /**
     *
     */
    protected Point loginNamePoint = new Point(frameDimension.width/2-150,60);

    /**
     *
     */
    protected Rectangle loginNameRectangle = new Rectangle(loginNamePoint,loginNameDimension);

    /**
     *
     */
    protected Dimension loginPassDimension = new Dimension(65,20); 

    /**
     *
     */
    protected Point loginPassPoint = new Point(frameDimension.width/2-150,100);

    /**
     *
     */
    protected Rectangle loginPassRectangle = new Rectangle(loginPassPoint,loginPassDimension);

    /**
     *
     */
    protected Dimension buttonDimension = new Dimension(160,40);

    /**
     *
     */
    protected Point accountPoint = new Point(100-buttonDimension.width/2,100);

    /**
     *
     */
    protected Rectangle accountRectangle = new Rectangle(accountPoint,buttonDimension);

    /**
     *
     */
    protected ActionAccount aAccount = new ActionAccount();

    /**
     *
     */
    protected Point guestPoint = new Point(frameDimension.width/2-buttonDimension.width/2,100);

    /**
     *
     */
    protected Rectangle guestRectangle = new Rectangle(guestPoint,buttonDimension);

    /**
     *
     */
    protected ActionGuest aGuest = new ActionGuest();

    /**
     *
     */
    protected Point createPoint = new Point(500-buttonDimension.width/2,100);

    /**
     *
     */
    protected Rectangle createRectangle = new Rectangle(createPoint,buttonDimension);

    /**
     *
     */
    protected ActionCreate aCreate = new ActionCreate();

    /**
     *
     */
    protected Dimension submitDimension = new Dimension(75,30);

    /**
     *
     */
    protected Point submitPoint = new Point(frameDimension.width/2-submitDimension.width/2,130);

    /**
     *
     */
    protected Rectangle submitRectangle = new Rectangle(submitPoint,submitDimension);

    /**
     *
     */
    protected ActionSubmit aSubmit = new ActionSubmit();

    /**
     *
     */
    protected Dimension loginDimension = new Dimension(75,30);

    /**
     *
     */
    protected Point loginPoint = new Point(frameDimension.width/2-submitDimension.width/2,130);

    /**
     *
     */
    protected Rectangle loginRectangle = new Rectangle(submitPoint,submitDimension);

    /**
     *
     */
    protected ActionLogin aLogin = new ActionLogin();

    /**
     *
     */
    protected Dimension signDimension = new Dimension(150,30);

    /**
     *
     */
    protected Point signPoint = new Point(frameDimension.width/2-signDimension.width/2,130);

    /**
     *
     */
    protected Rectangle signRectangle = new Rectangle(signPoint,signDimension);

    /**
     *
     */
    protected ActionSign aSign = new ActionSign();

    /**
     *
     */
    protected Dimension backDimension = new Dimension(80,30);

    /**
     *
     */
    protected Point backPoint = new Point(0,frameDimension.height-60);

    /**
     *
     */
    protected Rectangle backRectangle = new Rectangle(backPoint,backDimension);

    /**
     *
     */
    protected ActionBack aBack = new ActionBack();

    /**
     *
     */
    protected Dimension nameTextDimension = new Dimension(160,20); 

    /**
     *
     */
    protected Point nameTextPoint = new Point((frameDimension.width)/2-nameTextDimension.width/2,60);

    /**
     *
     */
    protected Rectangle nameTextRectangle = new Rectangle(nameTextPoint,nameTextDimension);

    /**
     *
     */
    protected Dimension passTextDimension = new Dimension(160,20); 

    /**
     *
     */
    protected Point passTextPoint = new Point(frameDimension.width/2-passTextDimension.width/2,100);

    /**
     *
     */
    protected Rectangle passTextRectangle = new Rectangle(passTextPoint,passTextDimension);
    private String playerName = null;
    private URL iconUrl = getClass().getResource("./img/gaming.png");
    private ImageIcon icon = new ImageIcon(iconUrl);
    
    /**
     *
     */
    public WelcomeGui() {
        this.init();
    }
    
    /**
     *
     */
    public void init(){
        this.setIconImage(icon.getImage());
        this.setSize(frameDimension);
        this.setTitle("BlackJack");
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        
        
        this.setContentPane(window);
        this.setLayout(null);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        
        //Labels
        //Welcome
        
        welcomeLabel.setBounds(welcomeRectangle);
        welcomeLabel.setHorizontalAlignment(JLabel.CENTER);
        welcomeLabel.setForeground(BUTTONCOLOR);
        //Info
        
        infLabel.setBounds(infRectangle);
        infLabel.setHorizontalAlignment(JLabel.CENTER);
        infLabel.setForeground(BUTTONCOLOR);
        //Name
       
       
        nameLabel.setBounds(nameRectangle);
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        nameLabel.setForeground(BUTTONCOLOR);
        //Waitng
        
        waitLabel.setBounds(waitRectangle);
        waitLabel.setHorizontalAlignment(JLabel.CENTER);
        waitLabel.setForeground(BUTTONCOLOR);
        //Loading

        loadingLabel.setBounds(loadingRectangle);
        
        //logName
        
        logNameLabel.setBounds(loginNameRectangle);
        logNameLabel.setHorizontalAlignment(JTextField.CENTER);
        logNameLabel.setForeground(BUTTONCOLOR);
        
        //logPass
        
        logPassLabel.setBounds(loginPassRectangle);
        logPassLabel.setHorizontalAlignment(JTextField.CENTER);
        logPassLabel.setForeground(BUTTONCOLOR);
        

        
        //Buttons
        
        accountButton.setBounds(accountRectangle);
        accountButton.setBackground(BUTTONCOLOR);
        accountButton.addActionListener(aAccount);
        //Guest
        
        guestButton.setBounds(guestRectangle);
        guestButton.setBackground(BUTTONCOLOR);
        guestButton.addActionListener(aGuest);
        //Create Player
        
        createButton.setBounds(createRectangle);
        createButton.setBackground(BUTTONCOLOR);
        createButton.addActionListener(aCreate);
        //Submit
        
        submitButton.setBounds(submitRectangle);
        submitButton.setBackground(BUTTONCOLOR);
        submitButton.addActionListener(aSubmit);
        //Login
        
        loginButton.setBounds(loginRectangle);
        loginButton.setBackground(BUTTONCOLOR);
        loginButton.addActionListener(aLogin);
        //Signup
        
        signButton.setBounds(signRectangle);
        signButton.setBackground(BUTTONCOLOR);
        signButton.addActionListener(aSign);
        //Back
        
        backButton.setBounds(backRectangle);
        backButton.setBackground(BUTTONCOLOR);
        backButton.addActionListener(aBack);
        //Text
        //NameText
        
        nameText.setBounds(nameTextRectangle);
        nameText.setHorizontalAlignment(JTextField.CENTER);
        nameText.setBackground(BUTTONCOLOR);
        nameText.setForeground(TABLECOLOR);
        //Password
        
        passText.setBounds(passTextRectangle);
        passText.setHorizontalAlignment(JTextField.CENTER);
        passText.setBackground(BUTTONCOLOR);
        passText.setForeground(TABLECOLOR);
        
        
       
        
        window.add(welcomeLabel);
        window.add(infLabel);
        window.add(accountButton);
        window.add(guestButton);
        window.add(createButton);
        

        this.setVisible(true);
    }
    
    /**
     *
     */
    public class Window extends JPanel{
        @Override
        public void paintComponent(Graphics g){
            g.setColor(TABLECOLOR);
            g.fillRect(0, 0, frameDimension.width, frameDimension.height);
        }
    }
    
    /**
     *
     */
    public class ActionAccount implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            window.removeAll();
            window.add(logNameLabel);
            window.add(logPassLabel);
            window.add(nameText);
            window.add(passText);
            window.add(loginButton);
            window.add(backButton);
            
            window.repaint();
        }
    }
    
    /**
     *
     */
    public class ActionGuest implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            window.removeAll();
            window.add(logNameLabel);
            window.add(submitButton);
            window.add(nameLabel);
            window.add(nameText);
            window.add(backButton);
            window.repaint();
            
            
        }
        
    }
    
    /**
     *
     */
    public class ActionCreate implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            window.removeAll();
            window.add(logNameLabel);
            window.add(logPassLabel);
            window.add(nameText);
            window.add(passText);
            window.add(signButton);
            window.add(backButton);
            window.repaint();
        }
        
    }
    
    /**
     *
     */
    public class ActionSubmit implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            window.removeAll();
            playerName = (nameText.getText());
            System.out.println(playerName);
            nameText.removeAll();
            window.removeAll();
            window.add(loadingLabel);
            window.add(waitLabel);
            window.repaint();
            //ClientRunner client = new ClientRunner();
        }
        
    }
    
    /**
     *
     */
    public class ActionLogin implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            window.removeAll();
            playerName = (nameText.getText());
            System.out.println(passText.getText());
            nameText.removeAll();
            window.removeAll();
            window.add(loadingLabel);
            window.add(waitLabel);
            window.repaint();
        }
        
   
    }
    
    /**
     *
     */
    public class ActionSign implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            window.removeAll();
            playerName = (nameText.getText());
            System.out.println(passText.getText());
            nameText.removeAll();
            window.removeAll();
            window.add(loadingLabel);
            window.add(waitLabel);
            window.repaint();
        }
        
   
    }
    
    /**
     *
     */
    public class ActionBack implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            window.removeAll();
            window.add(welcomeLabel);
            window.add(infLabel);
            window.add(accountButton);
            window.add(guestButton);
            window.add(createButton);
            window.repaint();
        }
        
   
    }

    /**
     *
     * @return
     */
    public String getPlayerName() {
        return playerName;
    }
    
    
    
    
  
        
    
   
    
}


