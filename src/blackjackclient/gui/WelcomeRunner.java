/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjackclient.gui;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author pbudahazy
 */
public class WelcomeRunner implements Runnable {
    Thread runner ;
    private final AtomicBoolean running = new AtomicBoolean(false);     
    WelcomeGui welcome ;

    /**
     *
     */
    public WelcomeRunner() {
        welcome = new WelcomeGui();
        
    }

    /**
     *
     */
    public void start(){
        runner = new Thread(this);
        runner.start();
        
    }
    
    /**
     *
     */
    public synchronized void stop(){
        running.set(false);
        welcome.setVisible(false);
        welcome.dispose();
    }
    
    @Override
    public void run() {
        
        running.set(true);
        while(running.get()){
            
        }
        
    }

    /**
     *
     * @return
     */
    public synchronized WelcomeGui getWelcome() {
        return welcome;
    }
    
    
    
}

