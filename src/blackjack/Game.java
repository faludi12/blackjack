/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import blackjack.card.Card;
import blackjack.card.Deck;
import blackjack.member.ClientHandler;
import blackjack.member.Dealer;
import blackjack.member.Member;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;

/**
 *
 * @author pbudahazy
 */
public final class Game {
    private final int PORT = 12345;
    private Scanner sc ;
    private String name ;
    private ServerSocket ss; 
    private LinkedList<ClientHandler> clients ;
    private LinkedList<ClientHandler> left;
    private LinkedList<ClientHandler> newArrivals;
    private LinkedList<Member> members ;
    private Member dealer;
    private Deck theDeck;
    private ConnectionHandler connectionHandler;

    /**
     * A szerver oldali logikáért felelős főosztály
     * @throws Exception
     */
    public Game() throws Exception {
        sc = new Scanner(System.in);
        clients = new LinkedList<>();
        members = new LinkedList<>();
        left = new LinkedList<>();
        newArrivals = new LinkedList<>();
        this.runGame();
    }
    
    /**
     * SzerverSocket beállítása
     * @throws IOException
     */
    public void setSS() throws IOException{
        ss = new ServerSocket(PORT);
    }
    
    /**
     * A csatlakozásért felelő függvény ,állítható időzitővel,ami utan elindul az első kör
     * @throws Exception
     */
    public void makeClients() throws Exception{
        ServerListener serverListener = new ServerListener(this);
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> future;  
        future = (Future<String>) executor.submit(serverListener);
        try {
            System.out.println("Server is listening..");
            System.out.println(future.get(20, TimeUnit.SECONDS));
            
        } catch (TimeoutException e) {
            future.cancel(true);
            System.out.println("Terminated!");
            serverListener.setInGame(true);
            
        }
        executor.shutdownNow();
        if(clients.isEmpty()){
            System.out.println("No connected player!\nBYE");
            System.exit(0);
        }
        System.out.println(clients.size() + " player is playing");
        
    }
    
    /**
     * A klienseket előállító függvény
     * @param client
     */
    public void addMembers(ClientHandler client){
        clients.add(client);
//        connectionHandler = new ConnectionHandler(this);
//        connectionHandler.start();
    }
    
    /**
     * A játék ideje alatt csatlakozott játékosok csatlaozásáért felelő függvény
     * @param client
     */
    public void addNewArrivals(ClientHandler client){
        newArrivals.add(client);
    }
    
    /**
     * A paklit és az osztót előállító függvény
     */
    public void makeDeckAndDealer(){
        dealer = new Dealer();
        theDeck = new Deck(1,true);
    }
    
    /**
     * A játékosok adatainak beállításáért felelős függvény
     * @throws IOException
     */
    public void setPlayersData() throws IOException{
        clients.forEach((myClient) -> {
            members.add(myClient.getPlayer());
        });
        clients.forEach((myClient) -> {
            myClient.setMembers(members);
        });
        for(ClientHandler myClient : clients){myClient.sendClientsName();}
        members.forEach((member) -> {
            member.setClientList(clients);
        });
        dealer.setClientList(clients);  
    }
    
    /**
     * A játék közben csatlakozó játékosok adatainak beállításáért felelős függvény
     * @throws IOException
     */
    public void setNewArrivalsData() throws IOException{
        if(!newArrivals.isEmpty()){
            newArrivals.forEach((myClient) -> {
            members.add(myClient.getPlayer());
            });
            newArrivals.forEach((myClient) -> {
                myClient.setMembers(members);
            });

            clients.addAll(newArrivals);
        }
        
        
        for(ClientHandler myClient : clients){myClient.sendClientsName();}
        members.forEach((member) -> {
            member.setClientList(clients);
        });
        dealer.setClientList(clients);  
    }
    
    public void getBids() throws IOException, ClassNotFoundException{
        for(ClientHandler client : clients){
            client.proccesAnswer(theDeck);
        }
    }
    
    /**
     * Az első 2 lap osztásáért felelő függvény 
     * @throws IOException
     */
    public void dealFirstCards() throws IOException{
//        members.forEach((member) -> {
//            member.setClientList(clients);
//        });
        dealer.setClientList(clients);
        for(int i = 0; i < 2 ; i++){
            for(Member member : members){member.addCard(theDeck.dealNextCard());}
            dealer.addCard(theDeck.dealNextCard());   
        }
        members.forEach((member) -> {
            member.printHand(true);
        });
        dealer.printHand(true);
        System.out.println("****************Cards are dealt****************     \n");
    }
    
    /**
     * A játékosok interakcióját feldogolgozó függvény
     * @param num klienslista hossza
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void waitAndPlay(int num) throws IOException, ClassNotFoundException{
        
            if(!(num<clients.size())){
                return;
            }
//            for(ClientHandler myClient : clients)
            
            try{
                clients.get(num).getPlayer().setActual(true);
                clients.get(num).proccesAnswer(theDeck);
                clients.get(num).getPlayer().printHand(true); 

                waitAndPlay(num+1);

            } catch (SocketException ex){
                clients.get(num).getPlayer().printHand(true);
                clients.get(num).getPlayer().setActual(false);
                System.out.println(clients.get(num).getPlayer().getName() + " left the game");
                left.add(clients.get(num));
                ClientHandler ch = clients.get(num);
                clients.remove(clients.get(num));
                if(!clients.isEmpty()){
                    clients.get(num).sendToClients(new Pair<>(ch.getPlayer().getName(),Card.LEAVE));
                }

                waitAndPlay(num);



            }  
            
        
    }
    
    /**
     * Az osztó körének a logikájért felelős függvény
     * @throws IOException
     * @throws InterruptedException
     */
    public void dealersTurn() throws IOException, InterruptedException{
        System.out.println("Dealer's turn\n");
        dealer.setActual(true);
        while(dealer.getHandsum() < 17){
            sendAllClients(new Pair<>(dealer.getName(),Card.HIT));
            Thread.sleep(1500);
            dealer.addCard(theDeck.dealNextCard());   
        }
        if(dealer.getHandsum()< 21 ){
           sendAllClients(new Pair<>(dealer.getName(),Card.STAY)); 
           return;
        }else{
            clients.getFirst().sendToClients(new Pair<>(dealer.getName(),Card.BUSTED));
            dealer.setBusted(true);
        }
        dealer.printHand(true);
    }
    
    /**
     * A győztesek játéosokat kerső függvény
     * @throws IOException
     * @throws InterruptedException
     */
    public void getWinners() throws IOException, InterruptedException{
        System.out.print("The Winner(s): ");
        boolean dealerWin = true;
        for(Member member : members){
            if(dealer.isBusted() || dealer.getHandsum() < member.getHandsum() && !member.isBusted() ){
                member.setWin(true);
                dealerWin = false;
                sendAllClients(new Pair<>(member.getName(),Card.WINNER));
                System.out.print(member.getName() + " ");
                
            }else if(member.getHandsum() == dealer.getHandsum() && !member.isBusted()){
                sendAllClients(new Pair<>(member.getName(),Card.DRAW));
            
                
            }else if(dealerWin && !dealer.isBusted()){
            dealer.setWin(true);
            sendAllClients(new Pair<>(dealer.getName(),Card.WINNER));
            System.out.print(dealer.getName() + " ");  
            }else{
            sendAllClients(new Pair<>("Nobody",Card.WINNER));
            }
        }
        
        
        
    }
    
    /**
     * Az összes kliens felé továbbítja a lapot ez a függvény
     * @param card A játéos nevéből illetve a neki huzott kártya(játék vagy parancs)
     * @throws IOException
     */
    public void sendAllClients(Pair<String,Card> card) throws IOException{
        for(ClientHandler client : clients){
            try{
                clients.getFirst().sendToClients(card);
            }catch(SocketException e){
                
            }
        }
    }
    
    /**
     * Az új játékkör indításáért felelő függvény
     * @throws IOException
     * @throws java.lang.InterruptedException
     * @throws java.lang.ClassNotFoundException
     */
    public void newGame() throws IOException, InterruptedException, ClassNotFoundException{
        Thread.sleep(1000*7);
        System.out.println("New Game");
        
        for(ClientHandler client : clients){
            client.proccesAnswer(theDeck);
            if(!client.getPlayer().isPlaying()){
                clients.remove(client);
            }else{
                client.getPlayer().setPlaying(false);
            }
        }
        
        
        if(clients.isEmpty()){
            noMorePlayer();
        }else{
            for(ClientHandler client : clients){
                client.getPlayer().emptyHand();
                client.getPlayer().setBusted(false);
                client.getPlayer().setWin(false);
            }
            dealer.emptyHand();
            dealer.setBusted(false);
            dealer.setWin(false);
            try{
                clients.getFirst().sendToClients(new Pair("",Card.NEWGAME));
            }catch(SocketException ex){
                noMorePlayer();
            }
            
            setNewArrivalsData();
            getBids();
            dealFirstCards();
            waitAndPlay(0);
            dealersTurn();
            getWinners();
        }
    }
    
    public void noMorePlayer(){
        System.out.println("Nobody is in the game");
        System.exit(0);
    }
    
    /**
     * SzerverSocket lekérése
     * @return
     */
    public ServerSocket getSs() {
        return ss;
    }
    
    /**
     * Kilépés a játékból
     */
    public void exitGame(){
        System.exit(0);
    }
    
    /**
     * Kliens törlésért felelős függvény
     * @param client A törlendő kliens
     * @throws IOException
     */
    public void removeClient(ClientHandler client) throws IOException{
        clients.remove(client);
        if(!clients.isEmpty()){
            clients.getFirst().sendToClients(new Pair<>(client.getPlayer().getName(),Card.LEAVE));
        }
        
        
    }

    /**
     * Klienslista lekérdezése
     * @return klienslista
     */
    public LinkedList<ClientHandler> getClients() {
        return clients;
    }
    
    /**
     * Játék logika
     * @throws IOException
     * @throws Exception
     */
    public void runGame() throws IOException, Exception{
        this.setSS();
        this.makeClients();
        this.makeDeckAndDealer();
        this.setPlayersData();
        this.getBids();
        this.dealFirstCards();
        this.waitAndPlay(0);
        this.dealersTurn();
        this.getWinners();
        while(true){
            this.newGame();
        }
        
    }


    
    
}
