/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import blackjack.member.ClientHandler;
import blackjackclient.gui.WelcomeGui;
import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pbudahazy
 */
public class ConnectionHandler implements Runnable {
    Thread runner ;
    private final AtomicBoolean running = new AtomicBoolean(false);     
    
    LinkedList<ClientHandler> clients;
    Game game ;

    /**
     *
     * @param game
     */
    public ConnectionHandler(Game game) {
        this.game = game;
        
    }
    
    
    
    private void connectionWatch() throws IOException, InterruptedException{
        ClientHandler thisClient = null ;
        for(ClientHandler client : game.getClients()){
            if(!client.getSocket().isBound()){
                thisClient = client;
            }
        }
        if(thisClient!=null){
            
            game.removeClient(thisClient);
        }
        if(game.getClients().isEmpty()){
            System.out.println("\nNo more player Bye");
            game.exitGame();
        }
        Thread.sleep(1000);
    }
    
    /**
     *
     */
    public void start(){
        runner = new Thread(this);
        runner.start();
        
    }
    
    /**
     *
     */
    public synchronized void stop(){
        running.set(false);
        
    }
    
    @Override
    public void run() {
        
        running.set(true);
        while(running.get()){
            try {
                try {
                    connectionWatch();
                } catch (InterruptedException ex) {
                    Logger.getLogger(ConnectionHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(ConnectionHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
