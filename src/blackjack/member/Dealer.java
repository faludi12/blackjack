/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.member;

import blackjack.card.Card;
import java.io.IOException;
import java.net.SocketException;
import javafx.util.Pair;

/**
 *
 * @author pbudahazy
 */
public class Dealer extends Member{

    /**
     *
     */
    public Dealer() {
        super();
        
                
    }
    
    /**
     *
     * @param card
     * @return
     * @throws IOException
     */
    @Override
    public boolean addCard(Card card) throws IOException{
        
        if(card.getNumber() > 0){
          hand.add(card);  
        }
        Pair<String , Card> pair = new Pair(name,card);
        for(ClientHandler client : clientList){
            try{
               client.writeOutput(pair); 
            }catch(SocketException e){
                System.out.println("Valami");
            }
            
        }
        return (this.getHandsum() < 17);
    }
    
    /**
     *
     * @return
     */
    @Override
    public int getHandsum() {
       int sum = 0;
       int numOfAces = 0;
       
       for(Card card : hand){
           if(card.getNumber() == 14){
                numOfAces++;
                sum += 11;
                
           }else if(card.getNumber() > 10){
               sum += 10;
           }else{
               sum += card.getNumber();
           }
       }
       while(sum > 21  && numOfAces > 0){
           sum -= 10; 
           numOfAces--;
       }
       
       return sum;
       
    }

    /**
     *
     * @param showFirstCard
     */
    @Override
    public void printHand(boolean showFirstCard){
        System.out.printf("%s's cards:\n",this.name);
        hand.forEach((card) -> {
            if(card.equals(hand.getFirst()) && !showFirstCard){
                System.out.println(" [hidden]");
            }else{
                System.out.printf(" %s\n",  card.toString());
            }
        });
    }
}
