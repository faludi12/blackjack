/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.member;

import blackjack.card.Deck;
import blackjack.card.Card;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Objects;
import javafx.util.Pair;

/**
 *
 * @author pbudahazy
 */
public class ClientHandler {
    
    private final Socket s ;
    private ObjectInputStream input = null;
    private ObjectOutputStream output = null;
    private String name = null;
    private final Member player;
    private LinkedList<Member> members;
    
    /**
     *
     * @param ss
     * @throws Exception
     */
    public ClientHandler(ServerSocket ss) throws Exception{

            s = ss.accept();
//            if(s.isConnected()){
//                System.out.println("The server is alive!");
//            }
            
            output = new ObjectOutputStream (s.getOutputStream());
            input = new ObjectInputStream(s.getInputStream());
            while(name  == null){
                name = (String)input.readObject();
            }
            System.out.println(name + " is connected!");
            player = createPlayer();
            
            

    }
    
    private Member createPlayer(){
        return new Player(name,this);
    }
    
    /**
     *
     * @param pair
     * @throws IOException
     */
    public void writeOutput(Pair<String,Card> pair) throws IOException{
        
        output.writeObject(pair);
        
        
    }
    
    /**
     *
     * @param pair
     * @throws IOException
     */
    public void sendToClients(Pair<String,Card> pair) throws IOException{
        for(ClientHandler thisClient : player.getClientList()){
            if(!(thisClient.equals(this)
                    && (pair.getValue().equals(Card.HIT)
                    || pair.getValue().equals(Card.STAY)
                    || pair.getValue().equals(Card.BUSTED)))){
                
                thisClient.writeOutput(pair);
            } 
            
        }
    }
    
    /**
     *
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Pair<String,String> readInput() throws IOException, ClassNotFoundException{
        Pair<String,String> answer = null ;
        while(answer == null){
            answer = (Pair<String,String>)input.readObject();
        }
        return answer;
    }
    
    /**
     *
     * @throws IOException
     */
    public void sendClientsName() throws IOException{
        LinkedList<String> names = new LinkedList<>();
        members.forEach((member) -> {
            names.add(member.getName());
        });
        output.writeObject(names);
        
    }
    
    /**
     *
     * @param deck
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void proccesAnswer(Deck deck) throws IOException, ClassNotFoundException{
        Member aMember;
        Pair<String,String> ans = this.readInput();
        aMember = this.findMemberByName(ans.getKey());
        if(ans.getValue().compareToIgnoreCase("H") == 0){
            this.sendToClients(new Pair<>(aMember.getName(),Card.HIT));
            if(aMember.addCard(deck.dealNextCard())){
               this.proccesAnswer(deck); 
            }else{
                aMember.setActual(false);
                aMember.setBusted(true);
            }
        }else if(ans.getValue().compareToIgnoreCase("S") == 0){
            this.sendToClients(new Pair<>(aMember.getName(),Card.STAY));
            aMember.setActual(false);
        }else if(ans.getValue().compareToIgnoreCase("B") == 0){
            this.sendToClients(new Pair<>(aMember.getName(),Card.BUSTED));
            aMember.setActual(false);
            aMember.setBusted(true);
        }else if(ans.getValue().compareToIgnoreCase("E") == 0){
            aMember.setPlaying(false);
            System.out.println(aMember.getName() + "left the game");
        }else if(ans.getValue().compareToIgnoreCase("P") == 0){
            aMember.setPlaying(true);
            System.out.println("Playing");
        }else{
            aMember.setBid(Integer.parseInt(ans.getValue()));
            System.out.println(ans.getKey() + "'s bid:" + ans.getValue());
        }
    }
    
    /**
     *
     * @param name
     * @return
     */
    public Member findMemberByName(String name){
        Member aMember = null ;
        for(Member member : members){
            if(member.getName().equals(name))
                aMember = member ;
        }
        return aMember;
    }

    /**
     *
     * @param members
     */
    public void setMembers(LinkedList<Member> members) {
        this.members = members;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    /**
     *
     * @return
     */
    public Socket getSocket() {
        return s;
    }
    
    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientHandler other = (ClientHandler) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.s, other.s)) {
            return false;
        }
        if (!Objects.equals(this.input, other.input)) {
            return false;
        }
        if (!Objects.equals(this.output, other.output)) {
            return false;
        }
        return Objects.equals(this.player, other.player);
    }

    /**
     *
     * @return
     */
    public Member getPlayer() {
        return player;
    }
    
    
    
    
    
    
    
}
