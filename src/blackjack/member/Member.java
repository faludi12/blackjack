/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.member;

import blackjack.card.Card;
import java.io.IOException;
import java.util.LinkedList;

/**
 *
 * @author pbudahazy
 */
public abstract class Member {
    
    /**
     *
     */
    protected String name;

    /**
     *
     */
    protected LinkedList <Card> hand;
    LinkedList<ClientHandler> clientList;
    private boolean actual = false;
    private boolean busted = false;
    private boolean win = false;
    private boolean playing = false;
    private int coins;
    private int bid = 0;
    
    /**
     *
     * @param name
     */
    protected Member(String name){
        
        this.name = name;
        hand = new LinkedList();
        coins = 50 ;
       

    }

    /**
     *
     */
    public Member() {
        this("Dealer");
    }
    
    /**
     *
     */
    public void emptyHand(){
        hand.clear();
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public void setBid(int bid) {
        this.bid = bid;
        coins -= bid;
    }
    
    

    
    
    public void loose(){
        
        System.out.println(getName() + "'s money: " + coins );
        bid = 0;
        if(coins < 0){
            System.out.println(getName() + " has no money!");
        }
    }
    
    public void win(){
        coins += 2 * bid;
        bid = 0;
        System.out.println(getName() + "'s money: " + coins );
    } 
    
    /**
     *
     * @param card
     * @return
     * @throws IOException
     */
    public abstract boolean addCard(Card card)throws IOException;

    /**
     *
     * @return
     */
    public abstract int getHandsum();

    /**
     *
     * @param showFirstCard
     */
    public abstract void printHand(boolean showFirstCard);

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     *
     * @param clients
     */
    public void setClientList(LinkedList<ClientHandler> clients) {
        this.clientList = clients;
    }

    /**
     *
     * @param act
     * @throws IOException
     */
    public void setActual(boolean act) throws IOException {
        this.actual = act;
        if(act){
            this.addCard(Card.ACTUAL);
        }
    }

    /**
     *
     * @return
     */
    public LinkedList<ClientHandler> getClientList() {
        return clientList;
    }

    /**
     *
     * @return
     */
    public boolean isBusted() {
        return busted;
    }

    /**
     *
     * @param busted
     */
    public void setBusted(boolean busted) {
        this.busted = busted;
        if(busted){loose();}
        
        
    }

    /**
     *
     * @return
     */
    public boolean isWin() {
        return win;
    }

    /**
     *
     * @param win
     */
    public void setWin(boolean win) {
        this.win = win;
        if(win){win();};
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public boolean isPlaying() {
        return playing;
    }
     
    

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     */
    public LinkedList<Card> getHand() {
        return hand;
    }

    /**
     *
     * @return
     */
    public boolean isActual() {
        return actual;
    }
    
    
    
    
    
    
    
    
    
    
}
