/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.member;

import blackjack.card.Card;
import java.io.IOException;
import javafx.util.Pair;

/**
 *
 * @author pbudahazy
 */
public class Player extends Member{
    
    
    ClientHandler client;
    final Card WINNER = new Card(-5);
    final Card BUSTED = new Card(-4);
    final Card HIT = new Card(-3);
    final Card STAY = new Card(-2);
    final Card ACTUAL = new Card(-1);

    /**
     *
     * @param name
     * @param client
     */
    public Player(String name,ClientHandler client) {
        super(name);
        this.client = client;
    }
    
    //Erdemes-e CardList-et kuldeni?

    /**
     *
     * @param card
     * @return
     * @throws IOException
     */
    @Override
    public boolean addCard(Card card) throws IOException{
        if(card.getNumber() > 0){
          hand.add(card);  
        }
        Pair<String , Card> pair = new Pair(name,card);
        client.sendToClients(pair);
        return (this.getHandsum() <= 21);
    }
    
    /**
     *
     * @return
     */
    @Override
    public int getHandsum() {
       int sum = 0;
       int numOfAces = 0;
       
       for(Card card : hand){
           if(card.getNumber() == 14){
                numOfAces++;
                sum += 11;    
           }else if(card.getNumber() > 10){
               sum += 10;
           }else{
               sum += card.getNumber();
           }
       }
       while(sum > 21 && numOfAces > 0){
           sum -= 10; 
           numOfAces--;
       }
       
       return sum;
       
    }
    
    /**
     *
     * @param showFirstCard
     */
    @Override
    public void printHand(boolean showFirstCard){
        System.out.printf("%s's cards:\n",this.name);
        hand.forEach((card) -> {
            if(card.equals(hand.getFirst()) && !showFirstCard){
                System.out.println(" [hidden]");
            }else{
                System.out.printf(" %s\n",  card.toString());
            }
        });
    }

    
    
    
}
