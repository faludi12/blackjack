/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import blackjack.member.ClientHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pbudahazy
 */
public class ServerListener implements Runnable{

    private Game game;
    boolean inGame ;

    /**
     * Kapcsolódások kezelése
     * @param game játék logia
     */
    public ServerListener(Game game) {
        this.game = game;
        inGame = false;
    }

    /**
     * már játszanak-e mások
     * @param inGame már játszanak-e mások
     */
    public synchronized void setInGame(boolean inGame) {
        this.inGame = inGame;
    }
    
    
    
    
    @Override
    public void run(){
        int clientCount = 0;
        //System.out.println("Server is listening");
        while(clientCount < 1){
            ClientHandler client = null;
            try {
                client = new ClientHandler(game.getSs());
            } catch (Exception ex) {
                Logger.getLogger(ServerListener.class.getName()).log(Level.SEVERE, null, ex);
            }
                if(!inGame){
                   game.addMembers(client); 
                }else{
                    game.addNewArrivals(client);
                }
                
                clientCount++;
        }
        
    }

    
    

}
