/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.card;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

/**
 *
 * @author pbudahazy
 */
public class Card implements Serializable{
    
    private Suit mySuit;
    private int myNumber;
    private Timestamp timestamp;
    
    public final static Card DRAW = new Card(-8);
    
    /**
     * Játék elhagyása parancs lap
     */
    public final static Card LEAVE = new Card(-7);

    /**
     * Új játék parancs lap
     */
    public final static Card NEWGAME = new Card(-6);

    /**
     * Győztes parancs lap
     */
    public final static Card WINNER = new Card(-5);

    /**     
     * Vesztes parancs lap
     */
    public final static Card BUSTED = new Card(-4);

    /**
     * Lapkérés parancs lap
     */
    public final static Card HIT = new Card(-3);

    /**
     * Megállás paracs lap
     */
    public final static Card STAY = new Card(-2);

    /**
     * Aktuális játkos parancs lap
     */
    public final static Card ACTUAL = new Card(-1);

    /**
     * Parancskártya létrehozása
     * @param aNumber kártyához tartozó egészszám aNumber∈[-7,-1]
     */
    public Card(int aNumber){
        
           this.mySuit = Suit.Commmand; 
           myNumber = aNumber;
        
        
        
        
    }
    
    /**
     * JátékKártya létrehozása
     * @param aSuit kárty mintája 
     * @param aNumber kártyához tartozó egészszám aNumber∈[2,14]
     */
    public Card(Suit aSuit,int aNumber){
        this.mySuit = aSuit;
        if(aNumber > 1  && aNumber < 15 ){
           this.myNumber = aNumber; 
           timestamp = new Timestamp(System.currentTimeMillis());
        }else {
            System.err.println(aNumber + "Is not a valid number");
            System.exit(1);
        }
            
    }
    
    /**
     * Kártya számának a lekérdezése
     * @return Kártya száma
     */
    public int getNumber(){
        return myNumber;
    }

    public Suit getSuit() {
        return mySuit;
    }
    
    
    
    @Override
    public String toString(){
        
        String numStr = "ERROR";
        
        switch(this.myNumber){
            
            case -7: 
                numStr = "Leave";
                break;
            
            
            case -6: 
                numStr = "New Game";
                break;
            
            case -5: 
                numStr = "Winner";
                break;
                
            case -4: 
                numStr = "Busted";
                break;
                
            case -3: 
                numStr = "Hit";
                break;
                
            case -2: 
                numStr = "Stay";
                break;
                
            case -1: 
                numStr = "Your Turn";
                break;
            
            case 2:
                numStr = "Two";
                break;
                
            case 3:
                numStr = "Three";
                break;
                
            case 4:
                numStr = "Four";
                break;
                
            case 5:
                numStr = "Five";
                break;
                
            case 6:
                numStr = "Six";
                break;
                
            case 7:
                numStr = "Seven";
                break;
                
            case 8:
                numStr = "Eight";
                break;
                
            case 9:
                numStr = "Nine";
                break;
                
            case 10:
                numStr = "Ten";
                break;
                
            case 11:
                numStr = "Jack";
                break;
                
            case 12:
                numStr = "Queen";
                break;
                
            case 13:
                numStr = "King";
                break;
                
            case 14:
                numStr = "Ace";
                break;
            
            
        }
        
        return numStr + " of " + mySuit.toString();
        
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Card other = (Card) obj;
        if (this.myNumber != other.myNumber) {
            return false;
        }
        if (this.mySuit != other.mySuit) {
            return false;
        }
        return Objects.equals(this.timestamp, other.timestamp);
    }

    

    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + Objects.hashCode(this.mySuit);
        hash = 13 * hash + this.myNumber;
        return hash;
    }
    
    /**
     * Kártya képének elérésiútját az előállító függvény
     * @return kártya kép Url
     * @throws IOException
     */
    public String getImageUrl() throws IOException{
        
        String numStr = "ERROR";
        
        switch(this.myNumber){
            
            
            case 2:
                numStr = "2";
                break;
                
            case 3:
                numStr = "3";
                break;
                
            case 4:
                numStr = "4";
                break;
                
            case 5:
                numStr = "5";
                break;
                
            case 6:
                numStr = "6";
                break;
                
            case 7:
                numStr = "7";
                break;
                
            case 8:
                numStr = "8";
                break;
                
            case 9:
                numStr = "9";
                break;
                
            case 10:
                numStr = "10";
                break;
                
            case 11:
                numStr = "jack";
                break;
                
            case 12:
                numStr = "queen";
                break;
                
            case 13:
                numStr = "king";
                break;
                
            case 14:
                numStr = "ace";
                break;

        }
        String url = "./img/"+numStr + "_of_" + mySuit.toString().toLowerCase() + ".png";
        return url;
    }
    
    
    
}
