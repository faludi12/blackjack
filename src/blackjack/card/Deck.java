/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.card;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author pbudahazy
 */
public class Deck {
    
    private LinkedList<Card> myCards;
    private int numCards;
    
    /**
     *
     */
    public Deck(){
        
        this(1,false);
        
    }
    
    /**
     *
     * @param numDecks
     * @param shuffle
     */
    public Deck(int numDecks,boolean shuffle){
        this.numCards = numDecks *52;
        this.myCards = new LinkedList();
        
        for (int decks = 0 ; decks < numDecks; decks++ ){
            
            for (int suit = 0; suit < 4 ; suit++){

                for (int num = 1 ; num < 14 ; num++ ){
                        myCards.add(new Card(Suit.values()[suit],num + 1));
                }
            }
        }
        
        if (shuffle){
            this.shuffle(myCards);
            
        }
        
        
        
    }
    
    private void shuffle(LinkedList<Card> cards){
        Random rng = new Random();
        
        
        
        cards.forEach((card) -> {
            Collections.swap(cards,rng.nextInt(cards.size()),cards.indexOf(card));
        });
        
    }
    
    /**
     *
     * @return
     */
    public Card dealNextCard(){
        if(myCards.isEmpty()){addCardsToDeck();}
        return myCards.removeLast();
        
        
    }
    
    public void addCardsToDeck(){
        LinkedList<Card> newDeck = new LinkedList<>();
        for (int decks = 0 ; decks < 53; decks++ ){
            
            for (int suit = 0; suit < 4 ; suit++){

                for (int num = 1 ; num < 14 ; num++ ){
                        newDeck.add(new Card(Suit.values()[suit],num + 1));
                }
            }
        }
        
        
        this.shuffle(newDeck);
        myCards.addAll(newDeck);
        
            
        
    }
    
    /**
     *
     * @param toPrint
     */
    public void printOutDeck(int toPrint){
        
        for(Card card : myCards){
         if(toPrint == 0){
             break;
         }   
         System.out.println(card.toString());
         toPrint--;
            
        }
    }
}
