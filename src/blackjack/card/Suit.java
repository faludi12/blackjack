package blackjack.card;

/**
 * Színekeket reprezentáló felsoroló 
 * @author Faludi Peter
 */
public enum Suit {

    /**
     *
     */
    Clubs,

    /**
     *
     */
    Diamonds,

    /**
     *
     */
    Spades,

    /**
     *
     */
    Hearts,

    /**
     *
     */
    Commmand,
}
